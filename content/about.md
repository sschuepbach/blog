---
title: "About the Blog"
date: "2019-03-29"
modified: "2019-04-01"
---

Hello,

My name is Sebastian, and this is the space where I intend to drop all kind of thoughts about software engineering. I hold a master's degree in History and Philosophy and currently work as a full-stack developer at the [University Library of Basel](https://ub.unibas.ch).

Since we are only a small team, my chores at work vary quite much. I write code, preferably in Scala and Rust, but also in Java, Python, Javascript / Typescript, Bash and if need be Perl. At the same time I do a lot of basic devops stuff, sketch software architectures, do some data modelling and tweak the infrastructure. Finally, there are also my personal interests regarding software engineering like for instance functional programming.

I don't consider myself an expert in any of the mentioned fields. But by encountering a wide variety of open questions, obstacles and problems, I have the luck to be able to constantly learning new, exciting stuff. Primarily for not forgetting them, I decided to log some of these ventures. For myself, but hopefully also for you, esteemed reader.