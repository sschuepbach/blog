---
title: "Command line utilities I didn't know of: `paste`"
categories: ["coreutils", "command line"]
date: "2019-07-20T10:50:00+02:00"
---

Have you ever had two lists and you wanted to print their elements side by side? Well, if that's the case, then the `paste` command is for you.

<!-- more -->

 In fact, it's the opposite of the `cat` command: While `cat` concatenates the lines of multiple files _vertically_ (i.e. sequentally), `paste` assembles them _horizontally_. For instance, you have two lists:

```bash
# one.list
1
2
3
```

```bash
# two.list
a
b
c
```

With `paste one.list two.list`, you get this:

```bash
1   a
2   b
3   c
```

Apart from that, there are few options: With the `-d` flag you can set another delimiter than the default tab. `-s` transposes the result:

```bash
# paste -s one.list two.list
1   2   3
a   b   c
```

Simple but useful.